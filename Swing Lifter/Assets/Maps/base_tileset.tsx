<?xml version="1.0" encoding="UTF-8"?>
<tileset name="base_tileset" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../Spritesheet/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="WaterRound" tile="60"/>
  <terrain name="DirtRound" tile="578"/>
 </terraintypes>
 <tile id="2" terrain=",,,0">
  <objectgroup draworder="index">
   <object id="1" x="1" y="12" width="15" height="4.45455"/>
   <object id="2" x="2.09091" y="7.90909" width="13.8182" height="3.90909"/>
   <object id="3" x="3.09091" y="6.18182" width="12.8182" height="1.72727"/>
   <object id="4" x="4.18182" y="5.18182" width="11.9091" height="1.09091"/>
   <object id="6" x="5.09091" y="3.90909" width="11.0909" height="1.36364"/>
   <object id="7" x="5.72727" y="3" width="10.2727" height="0.909091"/>
   <object id="9" x="8.09091" y="2.09091" width="8" height="1.18182"/>
   <object id="11" x="11" y="1.09091" width="4.81818" height="1"/>
  </objectgroup>
 </tile>
 <tile id="3" terrain=",,0,0">
  <objectgroup draworder="index">
   <object id="2" x="-0.181818" y="1" width="15.9091" height="15"/>
  </objectgroup>
 </tile>
 <tile id="4" terrain=",,0,">
  <objectgroup draworder="index">
   <object id="1" x="-0.0909091" y="1" width="5.27273" height="14.9091"/>
   <object id="2" x="5.18182" y="2" width="2.81818" height="13.9091"/>
   <object id="3" x="8.09091" y="2.90909" width="1.81818" height="12.9091"/>
   <object id="5" x="10" y="4.09091" width="0.909091" height="11.8182"/>
   <object id="6" x="11" y="5" width="0.909091" height="10.8182"/>
   <object id="7" x="12" y="5.90909" width="0.909091" height="10.0909"/>
   <object id="8" x="12.8182" y="8" width="1" height="7.81818"/>
  </objectgroup>
 </tile>
 <tile id="59" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="1" y="0" width="14.7273" height="16.0909"/>
  </objectgroup>
 </tile>
 <tile id="60" terrain="0,0,0,0">
  <objectgroup draworder="index">
   <object id="2" x="-0.03125" y="-0.15625" width="16" height="16.2188"/>
  </objectgroup>
 </tile>
 <tile id="61" terrain="0,,0,">
  <objectgroup draworder="index">
   <object id="1" x="-0.181818" y="-0.0909091" width="15.2727" height="16.1818"/>
  </objectgroup>
 </tile>
 <tile id="116" terrain=",0,,">
  <objectgroup draworder="index">
   <object id="1" x="0.955556" y="-0.0444444" width="15" height="4.04444"/>
   <object id="2" x="1.95556" y="3.97778" width="14.0222" height="4.02222"/>
   <object id="3" x="11" y="8.04444" width="5.02222" height="7"/>
   <object id="4" x="7.97778" y="7.97778" width="3" height="6"/>
   <object id="5" x="5.97778" y="7.95556" width="2.04444" height="5"/>
   <object id="6" x="5" y="7.97778" width="0.955556" height="3.97778"/>
   <object id="7" x="4" y="7.97778" width="1" height="3"/>
   <object id="8" x="2.97778" y="7.93333" width="1.02222" height="2.06667"/>
  </objectgroup>
 </tile>
 <tile id="117" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="-0.0909091" y="-0.181818" width="15.9091" height="14.9091"/>
  </objectgroup>
 </tile>
 <tile id="118" terrain="0,,,">
  <objectgroup draworder="index">
   <object id="2" x="-0.0222222" y="-0.0222222" width="5.02222" height="14.9333"/>
   <object id="3" x="4.97778" y="-0.0222222" width="3" height="13.9778"/>
   <object id="4" x="8" y="0.0222222" width="2" height="12.8667"/>
   <object id="5" x="10" y="0" width="0.955556" height="11.9778"/>
   <object id="6" x="10.9556" y="0" width="1.02222" height="10.9556"/>
   <object id="7" x="11.9556" y="0" width="1.04444" height="9.93333"/>
   <object id="8" x="13.0222" y="-0.0222222" width="0.977778" height="8"/>
   <object id="9" x="14.0222" y="-0.0222222" width="0.933333" height="4"/>
  </objectgroup>
 </tile>
 <tile id="518" terrain="1,1,1,"/>
 <tile id="519" terrain="1,1,,1"/>
 <tile id="520" terrain=",,,1"/>
 <tile id="521" terrain=",,1,1"/>
 <tile id="522" terrain=",,1,"/>
 <tile id="532">
  <objectgroup draworder="index">
   <object id="5" x="0.181818" y="8.81818" width="15.6364" height="5.27273"/>
   <object id="6" x="1.09091" y="14.1818" width="13.6364" height="0.727273"/>
   <object id="7" x="3.09091" y="14.6364" width="9.36364" height="1.45455"/>
  </objectgroup>
 </tile>
 <tile id="575" terrain="1,,1,1"/>
 <tile id="576" terrain=",1,1,1"/>
 <tile id="577" terrain=",1,,1"/>
 <tile id="578" terrain="1,1,1,1"/>
 <tile id="579" terrain="1,,1,"/>
 <tile id="634" terrain=",1,,"/>
 <tile id="635" terrain="1,1,,"/>
 <tile id="636" terrain="1,,,"/>
 <tile id="643">
  <objectgroup draworder="index">
   <object id="3" x="4.72727" y="13.9091" width="6.36364" height="2"/>
  </objectgroup>
 </tile>
</tileset>
