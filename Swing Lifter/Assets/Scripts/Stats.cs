﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {
    private int currentLevel;
    private int currenteExp;
    public int[] levelUpExp;

    void Update() {
        if (currenteExp >= levelUpExp[currentLevel]) {
            currentLevel++;
        }
    }

    public void addExp(int exp) {
        currenteExp += exp;
    }
}