﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveOnLoadController : MonoBehaviour {
    private static bool isExist;

    private void Start() {
        if (!isExist) {
            isExist = true;
            DontDestroyOnLoad(transform.gameObject);
        }
        else {
            Destroy(gameObject);
        }
    }
}