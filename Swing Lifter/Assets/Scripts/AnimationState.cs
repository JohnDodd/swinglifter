﻿using System;
using UnityEngine;

public class AnimationState {
    public float animationTime;
    private bool isAnimating;
    private float timer;
    public Action onAnimationStart;
    public Action<float> onAnimationUpdate;
    public Action onAnimationEnd;

    public void animate() {
        if (!isAnimating) {
            onAnimationStart();
            isAnimating = true;
            timer = animationTime;
        }
    }

    public void update() {
        if (isAnimating) {
            timer -= Time.deltaTime;
            if (timer <= 0) {
                isAnimating = false;
                onAnimationEnd();
            }
            else {
                onAnimationUpdate(timer);
            }
        }
    }
}