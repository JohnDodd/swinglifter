﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTimer : MonoBehaviour {
	public float time;
	private float timer;

	private void Start () {
		timer = time;
	}

	private void Update () {
		timer -= time;
		if (timer <= 0) {
			Destroy(gameObject);
		}
	}
}
