﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float moveSpeed;
    private Animator animator;
    private Rigidbody2D rigidbody;
    public Vector2 velocity;
    public float stamina = 100f;
    private AnimationState weaponAnimation;

    private void Start() {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
        weaponAnimation = new AnimationState();
        weaponAnimation.animationTime = 0.5f;
        weaponAnimation.onAnimationStart = () => { animator.SetBool("isAttacking", true); };
        weaponAnimation.onAnimationUpdate = timeLeft => {  };
        weaponAnimation.onAnimationEnd = () => { animator.SetBool("isAttacking", false); };
    }

    private void Update() {
        weaponAnimation.update();
        updateControls();
    }

    private void updateControls() {
        var horizontalAxis = Input.GetAxisRaw("Horizontal");
        var verticalAxis = Input.GetAxisRaw("Vertical");
        var isMovingHorizontal = Math.Abs(horizontalAxis) > 0.5f;
        var isMovingVertical = Math.Abs(verticalAxis) > 0.5f;

        var isRunning = Input.GetButton("Sprint");
        var moveSpeedCalculated = moveSpeed;
        if (isRunning) {
            moveSpeedCalculated = moveSpeed * 1.5f;
            stamina -= Time.deltaTime * 10;
        }
        else {
            stamina += Time.deltaTime * 10;
        }

        stamina = stamina < 0 ? 0 :
            stamina > 100 ? 100 : stamina;

        velocity.x = isMovingHorizontal ? horizontalAxis * moveSpeedCalculated : 0f;
        velocity.y = isMovingVertical ? verticalAxis * moveSpeedCalculated : 0f;
        rigidbody.velocity = velocity;

        if (isMovingHorizontal) {
            animator.SetFloat("lastMoveX", horizontalAxis);
            animator.SetFloat("lastMoveY", 0f);
        }

        if (isMovingVertical) {
            animator.SetFloat("lastMoveY", verticalAxis);
            animator.SetFloat("lastMoveX", 0);
        }

        if (Input.GetButton("Fire1")) {
            weaponAnimation.animate();
        }

        animator.SetBool("isMoving", isMovingHorizontal || isMovingVertical);
        animator.SetFloat("moveX", horizontalAxis);
        animator.SetFloat("moveY", verticalAxis);
    }
}