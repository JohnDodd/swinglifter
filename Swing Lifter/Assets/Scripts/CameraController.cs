﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject followTarget;
    public float moveSpeed;

    void Start() {
    }

    void Update() {
        var transformPosition = followTarget.transform.position;
        var targetPosition = new Vector3(transformPosition.x, transformPosition.y, -10);
        transform.position = Vector3.Lerp(transform.position, targetPosition, moveSpeed * Time.deltaTime);
    }
}